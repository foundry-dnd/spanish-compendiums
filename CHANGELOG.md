# Changelog
## [Unreleased]


## [0.6.0] 2020-06-18
### Modificado
- Actualiza las clases a la nueva versión del sistema D&D de Foundry
- Completa clérigo base
- Añade nombre en inglés de conjuros de nivel 1 de clérigo

### Arreglado
- Daño de ballesta ligera
- Favor divino ahora aplica efecto y daño 1d4


## [0.5.0] 2020-03-23
### Añadido
- Mago: base y evocación, además del rasgo del resto de escuelas
- Bestias varias

### Modificado
- Añade el nombre inglés de algunos conjuros
- Añade la información de daño a la maza


## [0.4.1] 2020-03-23
### Arreglado
- Cambia todas las referencias por el nuevo nombre del módulo


## [0.4.0] 2020-03-22
### Añadido
- Conjuros de nivel 1 y 2
- Rasgo "Asesinar" de Asesino


### Modificado
- Añade DAE a todos los trasfondos y los mejora

### Arreglado
- Añade DAE al bardo
- Modifica el uso de ciertas ropas, haciendo que ya no aparezcan como usables


## [0.3.0] 2020-03-21
### Añadido
- Paladín: juramento de entrega
- Algunos conjuros de nivel 3, 4, 5 y 9

### Modificado
- Mejora la tabla de rasgos de pícaro y paladín
- Completa explorador (revisado) y druida
- Mejora guerrero, hechicero


## [0.2.0] 2020-03-17
### Añadido
- Bardo: base, colegio del conocimiento
- Guerrero: base, maestro del combate
- Hechicero: base
- Paladín: base
- Pícaro: base, ladrón, embaucador arcano
- Algunos conjuros de nivel 2


## [0.1.0] 2020-02-18
### Añadido
- Rasgos raciales
- Algunos trucos y conjuros de nivel 1
- Monje: base, camino de la mano abierta y camino del kensei
- Avances en otras clases y subclases
