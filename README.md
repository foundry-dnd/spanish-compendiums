Compendios en español para D&D 5e.


# Instalación
En la ventana de instalar módulos de FoundryVTT, pegar el siguiente enlace en la parte inferior donde pone **Manifiesto URL**:
https://gitlab.com/foundry-dnd/spanish-compendiums/-/raw/master/module.json

![Dónde insertar la URL](https://i.imgur.com/DzVNrKz.png)

# Progreso
## Clases
### Bárbaro
- [ ] BASE [0 %]
- [ ] Senda del beserker [0 %]
- [ ] Senda del guerrero totémico [0 %]
- [ ] Senda del fanático [0 %]
- [ ] Senda del guardián ancestral [0 %]
- [ ] Senda del heraldo de las tormentas [0 %]

### Bardo
- [x] BASE
- [x] Colegio del conocimiento
- [ ] Colegio del valor [0 %]
- [ ] Colegio de las espadas [0 %]
- [ ] Colegio del glamour [0 %]
- [ ] Colegio de los susurros [0 %]

### Brujo
- [ ] BASE [0 %]
- [ ] El infernal [0 %]
- [ ] El primigenio [0 %]
- [ ] El señor feérico [0 %]
- [ ] El celestial [0 %]
- [ ] El filo maléfico [0 %]

### Clérigo
- [x] BASE
- [ ] Dominio del conocimiento [0 %]
- [ ] Dominio del engaño [0 %]
- [ ] Dominio de la guerra [0 %]
- [ ] Dominio de la luz [0 %]
- [ ] Dominio de la naturaleza [0 %]
- [ ] Dominio de la tempestad [0 %]
- [ ] Dominio de la vida [20 %]
- [ ] Dominio de la forja [0 %]
- [ ] Dominio de la sepultura [0 %]

### Druida
- [x] BASE
- [ ] Círculo de la tierra [1 %]
- [ ] Círculo de la luna [1 %]
- [ ] Círculo del pastor [0 %]
- [ ] Círculo de los sueños [0 %]

### Explorador (Revisado)
- [x] BASE
- [ ] Cazador (Revisado) [10 %]
- [ ] Señor de las bestias (Revisado) [0 %]
- [ ] Acechador en la penumbra [0 %]
- [ ] Caminante del horizonte [0 %]
- [ ] Cazador de monstruos [0 %]

### Guerrero
- [x] BASE
- [ ] Campeón [0 %]
- [x] Maestro del combate
- [ ] Caballero arcano [0 %]
- [ ] Arquero arcano [0 %]
- [ ] Caballero [0 %]
- [ ] Samurái [0 %]

### Hechicero
- [x] BASE
- [ ] Linaje dracónico [95 %]
- [ ] Magia salvaje [50 %]
- [ ] Alma divina [0 %]
- [ ] Hechicería de la tormenta [0 %]
- [ ] Magia de las sombras [0 %]

### Mago
- [x] BASE
- [ ] Escuela de abjuración [5 %]
- [ ] Escuela de adivinación [5 %]
- [ ] Escuela de conjuración [5 %]
- [ ] Escuela de encantamiento [5 %]
- [x] Escuela de evocación
- [ ] Escuela de ilusionismo [5 %]
- [ ] Escuela de nigromancia [5 %]
- [ ] Escuela de transmutación [5 %]
- [ ] Magia de guerra [5 %]

### Monje
- [x] BASE
- [x] Camino de la mano abierta
- [ ] Camino de la sombra [0 %]
- [ ] Camino de los cuatro elementos [0 %]
- [ ] Camino del alma solar [0 %]
- [x] Camino del kensei
- [ ] Camino del maestro borracho [0 %]

### Paladín
- [x] BASE
- [x] Juramentro de entrega
- [ ] Juramento de los antiguos [0 %]
- [ ] Juramento de venganza [10 %]
- [ ] Juramento de conquista [0 %]
- [ ] Juramento de redención [0 %]

### Pícaro
- [x] BASE
- [x] Ladrón
- [ ] Asesino [0 %]
- [x] Embaucador arcano
- [ ] Batidor [0 %]
- [ ] Espadachín [0 %]
- [ ] Inquisitivo [0 %]
- [ ] Mente maestra [0 %]



## Rasgos raciales
- [x] Enano
- [x] Elfo
- [x] Mediano
- [x] Humano
- [x] Dracónido
- [x] Gnomo
- [x] Semielfo
- [x] Semiorco
- [x] Tiefling


## Conjuros
- [ ] Trucos [80 %]
- [ ] Nivel 1 [65 %]
- [ ] Nivel 2 [15 %]
- [ ] Nivel 3 [5 %]
- [ ] Nivel 4 [5 %]
- [ ] Nivel 5 [5 %]
- [ ] Nivel 6 [0 %]
- [ ] Nivel 7 [0 %]
- [ ] Nivel 8 [0 %]
- [ ] Nivel 9 [5 %]


## Bestiario
Pendiente
